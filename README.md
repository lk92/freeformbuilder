## Easy free form builder

### We can create as many forms and collect as many responses as you want, There are no restrictions on making forms

Looking for free forms?  We can help you make that free forms which helps you to get consumer insights and feedback from your employees. 

#### Our features:

* A/B Testing
* Form Conversion
* Form Optimization
* Branch logic
* Payment integration
* Third party integration
* Push notifications
* Multiple language support
* Conditional logic
* Validation rules
* Server rules
* Custom reports

###   Its easy to use the integrated form maker and it's free of charge.

All responses that are automatically collected by [free form builder](https://formtitan.com) and the report is automatically generated for you which can also be downloaded along with the results by our [free form builder](http://www.formlogix.com/)

Happy free form building!